package mjk.hacker.algorithms.implementation.CatAndMouseSolved;

import java.util.Scanner;

public class Solution {

	private static final Scanner scanner = new Scanner(System.in);
	
	static String catAndMouse(int x, int y, int z) {
		String answer = "Mouse C";
		
		if(Math.abs(x -z) < Math.abs(y - z)) {
			answer = "Cat A";
		}
		
		if(Math.abs(x -z) > Math.abs(y - z)) {
			answer = "Cat B";
		}
		
		return answer;
	}
	
	public static void main(String[] args) {

        int q = Integer.parseInt(scanner.nextLine());

        for (int qItr = 0; qItr < q; qItr++) {
            String[] xyz = scanner.nextLine().split(" ");

            int x = Integer.parseInt(xyz[0]);

            int y = Integer.parseInt(xyz[1]);

            int z = Integer.parseInt(xyz[2]);

            System.out.println(catAndMouse(x, y, z));
            
        }

        scanner.close();
	}

}
