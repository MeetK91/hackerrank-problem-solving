/**
 * Problem statement: https://www.hackerrank.com/challenges/day-of-the-programmer/problem
 */
package mjk.hacker.algorithms.implementation.DayOfTheProgrammerSolved;

public class Solution {

	static void getDayAndMonth(int year) {
		
		final int GREGORIAN = 1;
		final int JULIAN = -1;
		final int SWITCHYEAR = 0;
		int calendarType = SWITCHYEAR;
		
		int day;
		int month = 0;
		int input=256;
		int months[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
		int leapMonths[] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366};
		boolean isLeapYear = false;
		
		String dayString, monthString;
		
		if(year <= 1917)
			calendarType = JULIAN;
		else
			if(year >= 1919)
				calendarType = GREGORIAN;
		
		switch(calendarType) {
			
			case GREGORIAN :
				
				isLeapYear = (year%4 == 0) && (year%100 != 0) || (year%400 == 0);
				
				if(isLeapYear) {
					months = leapMonths;				
				}
				
				break;
				
			case JULIAN:
				
				isLeapYear = (year%4 == 0);
				
				if(isLeapYear) {
					months = leapMonths;
				}
				
				break;
				
			case SWITCHYEAR:
				
				System.out.println("26.09.1918");
				return;
				
		}
		
		while (input > 365) {
		    // Parse the input to be less than or equal to 365
		    if(isLeapYear) {
				input -= 366;
		    }
		    else {
		    	input -= 365;
		    }
		}

		while (months[month] < input) {
		    // Figure out the correct month.
		    month++;
		}
		monthString = Integer.toString(month);
		
		// Get the day thanks to the months array
		day = input - months[month - 1];
		dayString = Integer.toString(day);
		
		if (month < 10) {
			monthString = "0"+month;
		}
		
		if (day < 10) {
			dayString = "0"+day;
		}
		
		System.out.println(dayString + "." + monthString + "." + year);
		return;
	}
	
	public static void main(String[] args) {
		for(int i = 1700; i <= 2020; i++) {
			getDayAndMonth(i);
		}
	}
}