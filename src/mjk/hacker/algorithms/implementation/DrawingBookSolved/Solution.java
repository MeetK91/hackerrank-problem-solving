package mjk.hacker.algorithms.implementation.DrawingBookSolved;

import java.io.*;
import java.util.*;

public class Solution {

	/*
	 * Complete the pageCount function below.
	 */
	static int pageCount(int n, int p) {

		int totalFromFront = n/2;
		int targetFromFront = p/2;
		int targetFromBack = totalFromFront - targetFromFront;
        
		return Math.min(targetFromFront, targetFromBack);
        
	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {

		while (true) {
			int n = Integer.parseInt(scanner.nextLine());

			int p = Integer.parseInt(scanner.nextLine());

			System.out.println(n + " " + p);
			System.out.println(Integer.toString(pageCount(n, p)));

		}
//		scanner.close();
	}
}
