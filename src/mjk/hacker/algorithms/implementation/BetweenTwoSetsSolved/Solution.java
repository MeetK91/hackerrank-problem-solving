/**
 * Problem statement: https://www.hackerrank.com/challenges/between-two-sets/problem
 */
package mjk.hacker.algorithms.implementation.BetweenTwoSetsSolved;

import java.io.*;
import java.util.*;

public class Solution {

    /*
     * Complete the getTotalX function below.
     */
    static int getTotalX(int[] a, int[] b) {
        Arrays.sort(a);
        Arrays.sort(b);

        int n = a.length;
        int m = b.length;
        int lower_bound = a[n-1];
        int upper_bound = b[0];

        int count_x = 0;
        for(int i = lower_bound; i <= upper_bound; i++){
            int sum_mod = 0;
            for(int j = 0; j < n; j++){
                sum_mod += i % a[j];
            }
            
            for(int k = 0; k < m; k++){
                sum_mod += b[k] % i;
            }

            if(sum_mod == 0)
                ++count_x;
        }

        return count_x;

    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nm = scan.nextLine().split(" ");

        int n = Integer.parseInt(nm[0].trim());

        int m = Integer.parseInt(nm[1].trim());

        int[] a = new int[n];

        String[] aItems = scan.nextLine().split(" ");

        for (int aItr = 0; aItr < n; aItr++) {
            int aItem = Integer.parseInt(aItems[aItr].trim());
            a[aItr] = aItem;
        }

        int[] b = new int[m];

        String[] bItems = scan.nextLine().split(" ");

        for (int bItr = 0; bItr < m; bItr++) {
            int bItem = Integer.parseInt(bItems[bItr].trim());
            b[bItr] = bItem;
        }

        int total = getTotalX(a, b);

        bw.write(String.valueOf(total));
        bw.newLine();

        bw.close();
    }
}
