/**
 * Problem statement: https://www.hackerrank.com/challenges/migratory-birds/problem
 */

package mjk.hacker.algorithms.implementation.MigratoryBirdsSolved;

import java.io.*;
import java.util.*;

public class Solution {

    // Complete the migratoryBirds function below.
    static int migratoryBirds(int arCount) {

    	int returnIndex = arCount+1;
    	int returnValue = -1;
    	
    	Map<Integer, Integer> birdMap = new HashMap<Integer, Integer>(arCount);
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        for(int i = 0; i < arCount; i++) {
        	int bird = scanner.nextInt();
        	if(birdMap.containsKey(bird)) {
        		int count = birdMap.get(bird);
        		count++;
        		birdMap.put(bird, count);
        	}
        	else {
        		birdMap.put(bird, 1);
        	}
        	
        }
        
        Map<Integer, Integer> sortedBirdMap = new TreeMap<Integer, Integer>(birdMap);
        
        sortedBirdMap = sortByValue(sortedBirdMap);
        
        //printMap(sortedBirdMap);

        for(Map.Entry<Integer, Integer> entry : sortedBirdMap.entrySet()) {
        	
        	if(returnIndex >= arCount) {
        		returnIndex = entry.getKey();
        		returnValue = entry.getValue();
        		continue;
        	}
        	
        	if(returnValue < entry.getValue()) {
        		returnIndex = entry.getKey();
        		returnValue = entry.getValue();
        	}
        	
        }

        return returnIndex;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int arCount = scanner.nextInt();
        
        System.out.println(migratoryBirds(arCount));
        
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        scanner.close();
    }
    
    public static <K, V> void printMap(Map<K, V> map) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            System.out.println("Bird : " + entry.getKey()
                    + " Count : " + entry.getValue());
        }
    }
    
    private static Map<Integer, Integer> sortByValue(Map<Integer, Integer> unsortMap) {

        List<Map.Entry<Integer, Integer>> list =
                new LinkedList<Map.Entry<Integer, Integer>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            public int compare(Map.Entry<Integer, Integer> o1,
                               Map.Entry<Integer, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
        for (Map.Entry<Integer, Integer> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
}