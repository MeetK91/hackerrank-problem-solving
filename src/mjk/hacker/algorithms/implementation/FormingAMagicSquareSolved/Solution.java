package mjk.hacker.algorithms.implementation.FormingAMagicSquareSolved;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {

//	4 8 2
//	4 5 7
//	6 1 6
	
//  4 9 2
//  3 5 7
//  8 1 5
	
	private static final Scanner scanner = new Scanner(System.in);
	
	static int formingMagicSquare(int[][] s) {

		int[][][] allSquares = { { { 8, 1, 6 }, { 3, 5, 7 }, { 4, 9, 2 } }, // 1

				{ { 6, 1, 8 }, { 7, 5, 3 }, { 2, 9, 4 } }, // 2

				{ { 4, 9, 2 }, { 3, 5, 7 }, { 8, 1, 6 } }, // 3

				{ { 2, 9, 4 }, { 7, 5, 3 }, { 6, 1, 8 } }, // 4

				{ { 8, 3, 4 }, { 1, 5, 9 }, { 6, 7, 2 } }, // 5

				{ { 4, 3, 8 }, { 9, 5, 1 }, { 2, 7, 6 } }, // 6

				{ { 6, 7, 2 }, { 1, 5, 9 }, { 8, 3, 4 } }, // 7

				{ { 2, 7, 6 }, { 9, 5, 1 }, { 4, 3, 8 } },// 8
		};
		
		Integer[] cost = new Integer[8];

		for (int i = 0; i < 8; i++) {
			cost[i] = 0;
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					if (s[j][k] != allSquares[i][j][k]) {
						cost[i] += Math.abs(s[j][k] - allSquares[i][j][k]);
					}
				}
			}
		}

		return Arrays.asList(cost).stream().min(Integer::compare).get();

	}

	public static void main(String[] args) {
		
		int[][] s = new int[3][3];

        for (int i = 0; i < 3; i++) {
            String[] sRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 3; j++) {
                int sItem = Integer.parseInt(sRowItems[j]);
                s[i][j] = sItem;
            }
        }

        System.out.println(formingMagicSquare(s));

	}

}
