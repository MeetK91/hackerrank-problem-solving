/**
 * Problem statement: https://www.hackerrank.com/challenges/apple-and-orange/problem
 */
package mjk.hacker.algorithms.implementation.AppleAndOrangeSolved;

import java.util.*;

public class Solution {
    	// Complete the countApplesAndOranges function below.
    static void countApplesAndOranges(long s, long t, long a, long b, long[] apples, long[] oranges) {

        if(validateInput(s,t,a,b,apples,oranges)) {
        	
        	System.out.println(calculateFruitsOnHouse(s,t,a,apples));
        	System.out.println(calculateFruitsOnHouse(s,t,b,oranges));
        	
        }
        else {
            System.exit(1);
        }
    }
    
    static long calculateFruitsOnHouse(long houseStart, long houseEnd, long treeLocation, long[] fruitDistances) {
    	
    	long fruitsOnHouse = 0;
    	long fruitLocation;
    	
    	for(long d : fruitDistances) {
    		fruitLocation = treeLocation + d;
    		
    		if(fruitLocation >= houseStart && fruitLocation <= houseEnd) {
    			fruitsOnHouse++;
    		}
    	}
    	
    	return fruitsOnHouse;
    }
    
    static boolean validateInput(long s, long t, long a, long b, long[] apples, long[] oranges) {
        
        if(s < 1 || s > 100000) {
            return false;
        }
        
        if(t < 1 || t > 100000) {
            return false;
        }
        
        if(a < 1 || a > 100000) {
            return false;
        }
        
        if(b < 1 || b > 100000) {
            return false;
        }
        
        if(apples.length < 1 || apples.length > 100000) {
            return false;
        }
        
        if(oranges.length < 1 || oranges.length > 100000) {
            return false;
        }
        
        if(!(a < s && s < t && t < b)) {
            return false;
        }
        
        for(long d : apples) {
            if(d < -100000 || d > 100000) {
                return false;
            }
        }
        
        for(long d : oranges) {
            if(d < -100000 || d > 100000) {
                return false;
            }
        }
        
        return true;
    }
    
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] st = scanner.nextLine().split(" ");

        long s = Integer.parseInt(st[0]);

        long t = Integer.parseInt(st[1]);

        String[] ab = scanner.nextLine().split(" ");

        long a = Integer.parseInt(ab[0]);

        long b = Integer.parseInt(ab[1]);

        String[] mn = scanner.nextLine().split(" ");

        long m = Integer.parseInt(mn[0]);

        long n = Integer.parseInt(mn[1]);

        long[] apples = new long[(int) m];

        String[] applesItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (long i = 0; i < m; i++) {
            long applesItem = Integer.parseInt(applesItems[(int) i]);
            apples[(int) i] = applesItem;
        }

        long[] oranges = new long[(int) n];

        String[] orangesItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (long i = 0; i < n; i++) {
            long orangesItem = Integer.parseInt(orangesItems[(int) i]);
            oranges[(int) i] = orangesItem;
        }

        countApplesAndOranges(s, t, a, b, apples, oranges);

        scanner.close();
    }
}