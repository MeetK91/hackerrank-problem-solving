package mjk.hacker.algorithms.implementation.PickingNumbers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.Collectors;

class Result {

	/*
	 * Complete the 'pickingNumbers' function below.
	 *
	 * The function is expected to return an INTEGER. The function accepts
	 * INTEGER_ARRAY a as parameter.
	 */

	public static long pickingNumbers(List<Integer> a) {
		
		//WRONG SOLUTION - DOES NOT TAKE CARE OF SAME NUMBER SCENARIO
		Map<Integer, Long> countMap = a.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

		List<Integer> distincts = a.stream().distinct().collect(Collectors.toList());

		if (distincts.size() > 1) {
			distincts.sort(Comparator.naturalOrder());

			List<Long> counts = new ArrayList<Long>();

			for (int i = 1; i < distincts.size(); i++) {
				int j = distincts.get(i - 1);
				int k = distincts.get(i);

				if ((k - j) == 1) {
					counts.add(countMap.get(j) + countMap.get(k));
				}
			}

			return Collections.max(counts);
		}
		
		return 0;
	}
	
	public static long optimal(List<Integer> a) {
		int frequency[] = new int[101];
        int result = Integer.MIN_VALUE;

        for (int i = 0; i < a.size(); i++) {
            int index=a.get(i);
            frequency[index]++; //frequency[index]=frequency[index]+1
        }

        for (int i = 1; i <= 100; i++) {
            result = Math.max(result, frequency[i] + frequency[i - 1]);
        }
        
        System.out.println(Arrays.toString(frequency));
        
        return result;
	}

}

public class Solution {
	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

		Integer.parseInt(bufferedReader.readLine().trim());

		List<Integer> a = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" ")).map(Integer::parseInt)
				.collect(Collectors.toList());

		System.out.println(Result.pickingNumbers(a));
		System.out.println(Result.optimal(a));
//        bufferedWriter.write(String.valueOf(result));
//        bufferedWriter.newLine();

		bufferedReader.close();
//        bufferedWriter.close();
	}
}