package mjk.hacker.algorithms.implementation.SockMerchantSolved;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class SockMerchant {

	private static int pairs = 0;
    // Complete the sockMerchant function below.
    static int sockMerchant(int n, Integer[] ar) {
        List<Integer> list = Arrays.asList(ar);
        
        Map<Integer, Long> counters = list.stream()
        	     .collect(Collectors.groupingBy(l -> l, 
        	         Collectors.counting()));
        
        counters.entrySet().stream().forEach(v -> pairs += (Math.floor(v.getValue()/2)));

        return pairs;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter();

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        Integer[] ar = new Integer[n];

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        sockMerchant(n, ar);
//
//        bufferedWriter.write(String.valueOf(result));
//        bufferedWriter.newLine();
//
//        bufferedWriter.close();

        scanner.close();
    }
}
