package mjk.hacker.algorithms.implementation.CountingValleysSolved;

import java.util.Scanner;

public class Solution {

	private static final Scanner scanner = new Scanner(System.in);
	
	static int countingValleys(int n, String s) {
		
		int currentLevel = 0;
        int valleys = 0;

        for(char step : s.toCharArray()) {
            
            if(step == 'U') {
                currentLevel++;
                if(currentLevel == 0) {
                	valleys++;
                }
            }
            else {
                currentLevel--;
            }

        }

        return valleys;
		
//		int currentLevel = 0;
//        int valleys = 0;
//        char[] steps = s.toCharArray();
//        for(char step : steps) {
//            
//            int prevLevel = currentLevel;
//            if(step == 'U') {
//                currentLevel++;
//            }
//            else {
//                currentLevel--;
//            }
//            
//            if(prevLevel < 0 && currentLevel == 0) {
//                valleys++;
//            }
//        }
//
//        return valleys;
    }
	
	static String getAlphaNumericString(int n) 
    { 
  
        // chose a Character random from this String 
        String AlphaNumericString = "UD"; 
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(AlphaNumericString.length() 
                        * Math.random());
  
            // add Character one by one in end of sb 
            sb.append(AlphaNumericString 
                          .charAt(index)); 
        } 
        System.out.println(sb.toString());
        return sb.toString(); 
    } 
	
	public static void main(String[] args) {
		
		int n = Integer.parseInt(scanner.nextLine());
		
		for (int i = 0; i < n; i++) {
			countingValleys(n, getAlphaNumericString(n));
		}
		
	}
}