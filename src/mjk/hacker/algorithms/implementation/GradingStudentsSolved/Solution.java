/**
 * Problem statement: https://www.hackerrank.com/challenges/grading/problem
 */
package mjk.hacker.algorithms.implementation.GradingStudentsSolved;

import java.io.*;
import java.util.*;

public class Solution {

    static final double roundingInt = 5.0;
    /*
     * Complete the gradingStudents function below.
     */
    static int[] gradingStudents(int[] grades) {
    	
    	if(validateGrades(grades)) {
    		grades = roundGrades(grades);
    	}
    	else {
    		System.exit(1);
    	}
    	
		return grades;

    }
    
    static int[] roundGrades(int[] grades) {
    	
    	int upperLimitFactor, finalGrade;
    	
    	for(int i = 0; i < grades.length; i++) {
			if(grades[i] >= 38) { //Initiate rounding only if grade >= 38, else no change.
				
				upperLimitFactor = (int) Math.ceil(grades[i]/roundingInt);
				finalGrade = (int) (upperLimitFactor*roundingInt);
				
				grades[i] = (finalGrade - grades[i]) < 3 ? finalGrade : grades[i];
			}
    	}
        
        
        return grades;
    }
    
    static boolean validateGrades(int[] grades) {
    	
    	if(grades.length > 60) {
    		return false;
    	}
    	
    	for(int grade : grades) {
    		if(grade < 0 || grade > 100) {
    			return false;
    		}
    	}
    	
    	return true;
    }


    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = Integer.parseInt(scan.nextLine().trim());

        int[] grades = new int[n];

        for (int gradesItr = 0; gradesItr < n; gradesItr++) {
            int gradesItem = Integer.parseInt(scan.nextLine().trim());
            grades[gradesItr] = gradesItem;
        }

        int[] result = gradingStudents(grades);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
            bw.write(String.valueOf(result[resultItr]));

            if (resultItr != result.length - 1) {
                bw.write("\n");
            }
        }

        bw.newLine();

        bw.close();
    }
}