/**
 * Problem statement: https://www.hackerrank.com/challenges/candies/problem
 */
package mjk.hacker.algorithms.greedy.Candies;

import java.io.*;
import java.util.*;

public class Solution {

    // Complete the candies function below.
    static long candies(int n, int[] arr) {

        int[] candies = new int[n];
        
        candies[0] = 1;
        
        for(int i = 1; i < arr.length; i++) {
            
            candies[i] = 1;
            
            if(arr[i] < arr[i-1]) {
            	candies[i-1] = candies[i] + 1;
            }
            else {
                if(arr[i] > arr[i-1]) {
                	candies[i] = candies[i-1] + 1;
                }
            }
        }
        
        for(int i = n-2; i > -1; i--) {
            if(arr[i] > arr[i+1]) {
            	if(candies[i] <= candies[i+1]) {
            		candies[i] = candies[i+1] + 1;
            	}
            }
            else {
            	if(arr[i] < arr[i+1]) {
            		if(candies[i+1] <= candies[i]) {
            			candies[i+1] = candies[i]+1;
            		}
            	}
            }
        }
        
        long sum = 0;
        
        for(int c :  candies) {
            sum += c;
        }
        
        return sum;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            int arrItem = scanner.nextInt();
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
            arr[i] = arrItem;
        }

        long result = candies(n, arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}