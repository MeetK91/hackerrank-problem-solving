/**
 * Problem statement: https://www.hackerrank.com/challenges/a-very-big-sum/problem
 */
package mjk.hacker.algorithms.warmup.AVeryBigSum;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanIn;
        String numbersInput, numbersArray[];
        long sum = 0;
        
        scanIn = new Scanner(System.in);
        scanIn.nextLine();
        numbersInput = scanIn.nextLine();
        numbersArray = numbersInput.split(" ");
        scanIn.close();
        for(int i = 0; i < numbersArray.length; i++)
        {
           sum += Integer.valueOf(numbersArray[i]);
        }
        System.out.println(sum);
    }
}