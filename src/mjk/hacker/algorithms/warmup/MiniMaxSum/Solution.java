/**
 * Problem statement: https://www.hackerrank.com/challenges/mini-max-sum/problem
 */
package mjk.hacker.algorithms.warmup.MiniMaxSum;

import java.util.*;

public class Solution {

    // Complete the miniMaxSum function below.
    static void miniMaxSum(long[] arr) {

        long minSum = 0, maxSum = 0;
        
        List<Long> list = Arrays.asList(arr[0], arr[1], arr[2], arr[3], arr[4]);
        
        Collections.sort(list);
        
        for(int i = 0; i < list.size(); i++) {
            
            if(i != 0) {
                maxSum += (list.get(i)).longValue();
            }
            
            if(i != (list.size() - 1)) {
                minSum += (list.get(i)).longValue();
            }
            
        }
               
        System.out.println(minSum + " " + maxSum);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        long[] arr = new long[5];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < 5; i++) {
            long arrItem = Long.parseLong(arrItems[i]);
            arr[i] = arrItem;
        }

        miniMaxSum(arr);

        scanner.close();
    }
}