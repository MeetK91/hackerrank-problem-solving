/**
 * Problem statement: https://www.hackerrank.com/challenges/simple-array-sum/problem
 */
package mjk.hacker.algorithms.warmup.SimpleArraySum;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        int sum = 0;
        String numbersInput, numbersArray[];
        Scanner scanIn;
        scanIn = new Scanner(System.in);
        scanIn.nextLine();
        numbersInput = scanIn.nextLine();
        scanIn.close();
        numbersArray = numbersInput.split(" ");
        for(int iCtr = 0; iCtr < numbersArray.length; iCtr++)
        {
          //System.out.println(numbersArray[iCtr]);
          int num = Integer.valueOf(numbersArray[iCtr]);
          sum += num;
        }
        System.out.println(sum);
    }
}