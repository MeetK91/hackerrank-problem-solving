/**
 * Problem statement: https://www.hackerrank.com/challenges/birthday-cake-candles/problem
 */
package mjk.hacker.algorithms.warmup.BirthdayCakeCandles;

import java.io.*;
import java.util.*;

public class Solution {

    // Complete the birthdayCakeCandles function below.
    static int birthdayCakeCandles(List<Integer> ar) {

        Integer tallest = Collections.max(ar);
        int canBlowOut=0;
        
        for(Integer i : ar) {
            if(i.equals(tallest)) {
                canBlowOut++;
            }
        }

        return canBlowOut;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int arCount = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        List<Integer> ar = new ArrayList<Integer>(arCount);

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < arCount; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar.add(arItem);
        }

        int result = birthdayCakeCandles(ar);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}