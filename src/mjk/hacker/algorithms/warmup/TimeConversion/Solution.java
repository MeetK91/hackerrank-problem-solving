/**
 * Problem statement: https://www.hackerrank.com/challenges/time-conversion/problem
 */
package mjk.hacker.algorithms.warmup.TimeConversion;

import java.io.*;
import java.text.*;
import java.util.*;

public class Solution {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {

        try {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ssa");
            Date date = parseFormat.parse(s);
            return displayFormat.format(date);
        }
        catch(Exception exc) {
            return "";
        }
    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scan.nextLine();

        String result = timeConversion(s);

        bw.write(result);
        bw.newLine();

        bw.close();
    }
}