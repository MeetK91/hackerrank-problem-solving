/**
 * Problem statement: https://www.hackerrank.com/challenges/solve-me-first/problem
 */
package mjk.hacker.algorithms.warmup.SolveMeFirst;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		int a;
		int b;
		
		Scanner s = new Scanner(System.in);
		
		a = s.nextInt();
		b = s.nextInt();
		s.close();
		
		System.out.println(solveMeFirst(a, b));
	}
	
	static int solveMeFirst(int a, int b) {
		return a+b;
	}
}