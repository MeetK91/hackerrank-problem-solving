/**
 * Problem statement: https://www.hackerrank.com/challenges/staircase/problem
 */
package mjk.hacker.algorithms.warmup.Staircase;

import java.util.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanIn;
        int ladderHeight;
        
        scanIn = new Scanner(System.in);
        ladderHeight = scanIn.nextInt();
        scanIn.close();
        for(int i=1; i<=ladderHeight; i++)
        {
            for(int j=ladderHeight-i; j>0; j--)
            {
                System.out.print(" ");
            }
            for(int k=1; k<=i; k++)
            {
                System.out.print("#");
            }
            System.out.println();
        }
    }
}