/**
 * Problem statement: https://www.hackerrank.com/challenges/plus-minus/problem
 */
package mjk.hacker.algorithms.warmup.PlusMinus;

import java.util.*;

public class Solution {

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {

        int positive = 0, negative = 0, zero = 0;
        
        for(int a : arr) {

            if(a > 0) {
                positive++;
            }
            else {
                if(a < 0) {
                    negative++;
                }
                else {
                    zero++;
                }
            }
        }
        
        System.out.println(String.format("%.6f", (float) positive/arr.length));
        System.out.println(String.format("%.6f", (float) negative/arr.length));
        System.out.println(String.format("%.6f", (float) zero/arr.length));

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}