package mjk.hacker.ai.botbuilding;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BotSavesPrincess {

	private static String savePrincess(int botX, int botY, int princessX, int princessY) {
		return "";
	}
	
	private static void readGrid() {
	
		final char PRINCESS = 'm';
		final char BOT = 'p';
		int botX = -1, botY = -1;
		int princessX = -1, princessY = -1;
		int rowNumber = -1;
		
		Scanner sc = new Scanner(System.in);
		Pattern p = Pattern.compile("+[" + PRINCESS + BOT + "]");
		
		int gridN = sc.nextInt();
		sc.nextLine();
		
		for(int i = 0; i < gridN; i++) {
			
			String row = sc.nextLine();
			Matcher matcher = p.matcher(row);
			
			if(matcher.matches()) {
				rowNumber++;
				char[] gridRow = row.toCharArray();
				
				for(int j = 0; j < gridRow.length; j++) {
					
					if(gridRow[i] == PRINCESS) {
						princessX = rowNumber;
						princessY = j;
					}
					
					if(gridRow[i] == BOT) {
						botX = rowNumber;
						botY = j;
					}
				}
			}
			
		}
		
		savePrincess(botX, botY, princessX, princessY);
		sc.close();
	}
	
	public static void main(String[] args) {
		
		readGrid();
		
	}
	
}