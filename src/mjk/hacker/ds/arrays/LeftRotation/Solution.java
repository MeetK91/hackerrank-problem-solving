package mjk.hacker.ds.arrays.LeftRotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Result {

    /*
     * Complete the 'rotateLeft' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. INTEGER d
     *  2. INTEGER_ARRAY arr
     */

    public static List<Integer> rotateLeft(int d, List<Integer> arr) {
        
        List<Integer> newIndices = new ArrayList<Integer>(arr.size());
        List<Integer> rotatedArray = new ArrayList<Integer>(arr.size());
        
        for(int i = 0 ; i < arr.size(); i ++) {
        	rotatedArray.add(0);
        }
        
        for(int i = 0; i < arr.size(); i++) {
            newIndices.add(rotateIndexToLeft(i, d, arr.size()));
        }
        
        for(int i = 0; i < arr.size(); i++) {
            rotatedArray.set(newIndices.get(i), arr.get(i));
        }
        System.out.println(rotatedArray);
        return rotatedArray;
        
    }
    
    static int rotateIndexToLeft(int oldIndex, int noOfRotations, int size) {
        
    	int actualShift = noOfRotations % size;
        int newIndex = oldIndex - actualShift;
        
        return newIndex < 0 ? (size - Math.abs(newIndex)) : newIndex;
    }

}

public class Solution {
	
	static List<Integer> arr = Arrays.asList(1,2,3,4,5);
	
	public static void main(String[] args) {
		Result.rotateLeft(4, arr);
	}

}
