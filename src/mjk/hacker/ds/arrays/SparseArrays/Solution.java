package mjk.hacker.ds.arrays.SparseArrays;

import java.util.HashMap;

public class Solution {

    // Complete the matchingStrings function below.
    static int[] matchingStrings(String[] strings, String[] queries) {

        return methodTwo(strings, queries);

    }
    
    static int[] methodOne(String[] strings, String[] queries) {
        int[] count = new int[queries.length];
        int queryCount = 0;
        
        for(int i = 0; i < queries.length; i++) {
            
            queryCount = 0;
            
            for(int j = 0; j < strings.length; j++) {
                if(strings[j].equals(queries[i])) {
                    queryCount++;
                }
            }
            
            count[i] = queryCount;
            
        }
        
        return count;
    }
    
    static int[] methodTwo(String[] strings, String[] queries) {
        
        HashMap<String, Integer> countMap = new HashMap<String, Integer>();
        int[] queriesCount = new int[queries.length];
        
        for(int i = 0; i < strings.length; i++) {
            
            int count = 0;
            
            for(int j = 0; j < strings.length; j++) {
                if(strings[j].equals(strings[i])) {
                    count++;
                }
            }
            
            countMap.put(strings[i], count);    
        }
        
        for(int i = 0; i < queries.length; i++) {
            if(countMap.containsKey(queries[i])) 
                queriesCount[i] = countMap.get(queries[i]);
            else
                queriesCount[i] = 0;
        }
        
        return queriesCount;
        
    }

    public static void main(String[] args) {
    	
    	
    }
}
