package mjk.hacker.ds.arrays.ArrayManipulation;

public class Solution {

	static long arrayManipulation(int n, int[][] queries) {

		/*
		 * Don't track record each individual element and its changes, instead record the add the change at each start location and subtract the change at each end+1 location.
		 * Then, iterate over this record of changes, summing at each element. And record max.
		 * */

		int[] arr = new int[n+1];
		
		

		for (int i = 0; i < queries.length - 1; i++) {

			int a = queries[i][0];
			int b = queries[i][1];
			int incr = queries[i][2];
			arr[a] += incr; //add incr to every element starting from a index to end
			arr[b+1] -= incr; //subtract incr from every element starting from b index to end -> this evens out the extra addition from above step
		
		}
		
		long max = 0, sum = 0;
		
		for(int i : arr) {
			sum = sum + i;
			max = Math.max(sum, max);
		}
		

		return max;

	}

}
